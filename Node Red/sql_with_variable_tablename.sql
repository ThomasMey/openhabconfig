set @itemname = "HarmonyHubHarmonyHub";
set @tablename =(SELECT CONCAT('000', (SELECT items.itemId FROM openhab.items WHERE items.itemName like @itemname))) ;
set @sqltext = CONCAT('Select * From ', CONCAT('item',SUBSTRING(@tablename,-4)));
prepare stmt FROM @sqltext;
execute stmt;
deallocate prepare stmt;
